source $OMF_PATH/themes/omf-prompt-adealoe/local_aliases.fish
source $OMF_PATH/themes/omf-prompt-adealoe/local_config.fish

function fish_title
    echo $display_user@$display_hostname
end

function fish_greeting
    echo (set_color brblue)\n"Welcome $display_user!"\n(set_color brblack)"$USER@$hostname"
end

if [ (id -u) = "0" ];
    alias rm "rm -i"
end
