General Notes:

This article helped me set up multiple ssh keys for different github & bitbucket accounts:
https://www.keybits.net/post/automatically-use-correct-ssh-key-for-remote-git-repo/

This alone wasn't enough though, I had to do a lot with ssh-agent.  
For my mac, I added the follwing things.  Not including it on the main repo because I don't know how well it'll play with other distros.


********  local_config.fish  ********

function clear-ssh-agent
    killall ssh-agent 2> /dev/null
    set -el SSH_AUTH_SOCK 2> /dev/null;
    set -eg SSH_AUTH_SOCK 2> /dev/null;
    set -eU SSH_AUTH_SOCK 2> /dev/null;
    set -el SSH_AGENT_PID 2> /dev/null;
    set -eg SSH_AGENT_PID 2> /dev/null;
    set -eU SSH_AGENT_PID 2> /dev/null;
end

function soft-clear-ssh-agent
    set -eg SSH_AUTH_SOCK 2> /dev/null;
    set -eg SSH_AGENT_PID 2> /dev/null;
end

function start-ssh-agent
    eval (string replace 'setenv' 'set -Ux' (ssh-agent -c))&
end

set -g display_hostname (hostname|cut -d . -f 1)
set -g display_user $USER

if not set -Uq SSH_AGENT_PID
    clear-ssh-agent
    start-ssh-agent
else
    soft-clear-ssh-agent
end



********  local_aliases.fish  ********

alias l_ssh-agent "clear-ssh-agent; start-ssh-agent"


Also,
Until I'm able to find a better solution, type the following two commands in after cloning the repository.
This will keep the files local_config.fish and local_aliases.fish from being tracked.

git update-index --skip-worktree local_aliases.fish
git update-index --skip-worktree local_config.fish
