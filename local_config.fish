# The following two variables will be used to display the username and host in the window title
# and the prompt.  In the case you wish to override the either, this is a safe way to do so.
# For example, the host 'a2ss14' is obscure to me, so I'd prefer it display "a2-vps"

if not set -q hostname
    set -g hostname (hostname)
end

set -g display_hostname (hostname|cut -d . -f 1)
set -g display_user $USER

# Make ls's colors work with GRC
export CLICOLOR=1
export CLICOLOR_FORCE=1
alias ls "grc --colour=auto ls -alh"

if not set -q fish_user_paths
    set -g fish_user_paths $PATH /Users/Adam/scripts .
end
