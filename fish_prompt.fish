function __user_colors
	if [ (id -u) = "0" ];
		set -g user_color (set_color --bold red)
		set -g bracket_color (set_color --bold red)
	else
		set -g user_color (set_color --bold 00bfff)
		set -g bracket_color (set_color brblack)
	end
end

function __user_host
	echo -n $user_color$display_user(set_color brblack)@(set_color green)$display_hostname(set_color normal)
	set -e user_color
end

function __current_path
	echo -n (set_color --bold blue)(pwd)(set_color normal)
end

function _git_branch_name
	echo (command git symbolic-ref HEAD 2> /dev/null | sed -e 's|^refs/heads/||')
end

function _git_is_dirty
	echo (command git status -s --ignore-submodules=dirty 2> /dev/null)
end

function _git_is_behind
	echo (command git status 2> /dev/null)
end

function __git_status
	if [ (_git_branch_name) ]
		set -l git_branch (_git_branch_name)

		set -g git_behind ""

		if [ (string match -r "behind" (_git_is_behind)) ]
			set -g git_behind (set_color --bold red)
		else
			set -g git_behind (set_color brblack)
		end

		if [ (_git_is_dirty) ]
			set -g info_color (set_color --bold red)
		else
			set -g info_color (set_color --bold green)
		end
		echo -n ' '$git_behind'::'$info_color$git_branch$git_behind'::'(set_color --bold normal)
		set -e info_color
	end
end

function dateTime
	echo (set_color --bold brblack)(date "+%Y-%m-%d% %H:%M:%S")(set_color normal)
	# echo "hi"
end

function fish_prompt
	__user_colors
	echo ""
	echo -n	"$bracket_color⎛"(set_color --bold brblack)"❨"(set_color normal)
	__user_host
	echo -n (set_color brblack)":"
	__current_path
	__git_status
	echo -n (set_color --bold brblack)"❩"
	echo ""
	echo $bracket_color"⎝"(set_color --bold brblack) (dateTime) (set_color 333333)⊸(set_color 991919)⊸(set_color FF0000)'≻' (set_color normal)
	set -e bracket_color
end
