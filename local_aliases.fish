#!/usr/local/bin/fish
alias su 'sudo -s'
alias wget 'curl -O'

alias lsd "ls -alhG | egrep '^d'"
alias lsf "ls -alhG | egrep '^[^d]'"

alias cdc 'omf reload'

alias a2 'ssh -p 7822 adealoe@adamdealoe.com;cdc'
alias ap 'ssh -p 7822 animuspro@original.animuspro.com;cdc'
alias apr 'ssh -p 7822 root@original.animuspro.com;cdc'
alias atip 'ssh -p 7822 atiporg@atip.org;cdc'
alias files 'ssh ersfiles@files.sysers.com;cdc'
alias routing 'ssh routing@files.sysers.com;cdc'
alias web1 'ssh ershost@ourers.com;cdc'
alias filesr 'ssh root@files.sysers.com;cdc'
alias web1r 'ssh root@ourers.com;cdc'
alias serenity 'ssh -p 7822 adam@serenity;cdc'
alias serenityl 'ssh adam@serenityl;cdc'
alias do1 'ssh animus@do1;cdc'
alias do1r 'ssh root@do1;cdc'

